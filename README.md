# Mob-Chat-App

This is a chat app build using firebase and react.

## For running app locally follow below steps
1. Clone the repo
2. run "npm install"
3. use firebase initial setup code in .env file
4. run "npm start"