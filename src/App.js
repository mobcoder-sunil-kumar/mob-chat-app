import "./App.css";

import React from "react";
import { BrowserRouter as Router } from "react-router-dom";
import Routes from "./navigation/Routes";
import Header from './navigation/Header'

function App() {
  // const [loading, setLoading] = useState(true);
  // if (loading) return <h2>Loading...</h2>;
  return (
    <div className="App">
      <Router>
        <Header />
        <Routes />
      </Router>
    </div>
  );
}

export default App;
