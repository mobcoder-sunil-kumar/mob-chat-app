import React from "react";
import { Button } from '@material-ui/core'


export default function FormButton() {
  return (
    <Button variant="outlined" size="large" color="primary" type="submit" style={{marginBottom:40}}>
      Submit
    </Button>
  );
}
