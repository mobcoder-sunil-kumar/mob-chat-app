import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { auth } from '../services/firebase';
import { signout } from "../helpers/auth";


const LoginButton = () => {
    const [authenticated, setAuthenticated] = useState(false);
    const history = useHistory();
    useEffect(() => {
      auth().onAuthStateChanged((user) => {
        if (user) {
          setAuthenticated(true);
        } else {
          setAuthenticated(false);
        }
      });
    }, []);

    const handleLogin = () => {
        if(authenticated) {
          signout();
        }
        history.push("/dashboard")
    }
    return (
        <button onClick={handleLogin} >{ authenticated ? " Logout" : " Login" }</button>
    )
}

export default LoginButton
