import React from "react";
import { Link } from "react-router-dom";
import "./Header.css";
import LoginButton from "../components/LoginButton";


export default function Links() {
  const [click, setClick] = React.useState(false);
  const handleClick = () => setClick(!click);
  const Close = () => setClick(false);
  return (
    <div>
      <div className={click ? "main-container" : ""} onClick={() => Close()} />
      <nav className="navbar" onClick={(e) => e.stopPropagation()}>
        <div className="nav-container">
          <Link exact to="/" className="nav-logo">
            Mob-Chat-App
            <i className="fa fa-code"></i>
          </Link>

          <ul className={click ? "nav-menu active" : "nav-menu"}>
            <li className="nav-item">
              <Link
                exact
                to="/"
                activeClassName="active"
                className="nav-links"
                onClick={click ? handleClick : null}
              >
                Home
              </Link>
            </li>
            <li className="nav-item">
              <Link
                exact
                to="/dashboard"
                activeClassName="active"
                className="nav-links"
                onClick={click ? handleClick : null}
              >
                Dashboard
              </Link>
            </li>
            <li className="nav-item">
              <LoginButton />
            </li>
          </ul>
          <div className="nav-icon" onClick={handleClick}>
            <i className={click ? "fa fa-times" : "fa fa-bars"}>menu</i>
          </div>
        </div>
      </nav>
    </div>
  );
}
