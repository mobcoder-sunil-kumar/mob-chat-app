import React, {useEffect, useState } from "react";
import {Switch, Route} from 'react-router-dom';
import { auth } from "../services/firebase";
import PrivateRoute from "./PrivateRoute";
import PublicRoute from "./PublicRoute";

import {Chat, Dashboard, Home, Login, Signup } from '../screens'

const Routes = () => {
    const [authenticated, setAuthenticated] = useState(false);
    useEffect(() => {
      auth().onAuthStateChanged((user) => {
        if (user) {
          setAuthenticated(true);
        //   setLoading(false);
        } else {
          setAuthenticated(false);
        //   setLoading(false);
        }
      });
    }, []);
  return (
    <Switch>
      <Route exact path="/" component={Home}></Route>
      <PrivateRoute
        path="/chat"
        authenticated={authenticated}
        component={Chat}
      ></PrivateRoute>
      <PrivateRoute
        path="/dashboard"
        authenticated={authenticated}
        component={Dashboard}
      ></PrivateRoute>
      <PublicRoute
        path="/signup"
        authenticated={authenticated}
        component={Signup}
      ></PublicRoute>
      <PublicRoute
        path="/login"
        authenticated={authenticated}
        component={Login}
      ></PublicRoute>
    </Switch>
  );
};

export default Routes;
