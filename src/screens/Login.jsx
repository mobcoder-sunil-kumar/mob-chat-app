import React, { useState } from "react";
import { Link } from "react-router-dom";
import { signin } from "../helpers/auth";
import { Formik, Form } from "formik";
import FormTextField from "../components/FormTextField";
import FormButton from "../components/FormButton";
import {loginSchema} from "../helpers/validation";

export default function Login() {
  const [error, setError] = useState("");
  const [email, setEmail] = useState("");
  const [pass, setPass] = useState("");

  async function handleLogin(e) {
    e.preventDefault();
    try {
      console.log('trigged')
      await signin(email, pass);
      console.log(email, pass)
    } catch (err) {
      setError(err);
    }
  }

  return (
    <div>
      <h1 style={{ marginBottom: 200 }}>
        Login to
        <Link to="/">mob-chat</Link>
      </h1>
      <input onChange={(e) => setEmail(e.target.value)} type="email" value={email} />
      <input onChange={(e) => setPass(e.target.value)}  type="password" value={pass} />
      <button onClick={handleLogin}>Login</button>
      {/* <Formik
        initialValues={{ email: "", password: "" }}
        onSubmit={handleLogin}
        validationSchema={loginSchema}
      >
        {() => (
          <Form>
            <FormTextField name="email" label="Email" type="email" />
            <FormTextField name="password" label="Password" type="password" />
            <FormButton />
          </Form>
        )}
      </Formik> */}
      {error ? <div>Error</div> : null}
      <div>
        Don't have account <Link to="/signup">Signup</Link>
      </div>
    </div>
  );
}
