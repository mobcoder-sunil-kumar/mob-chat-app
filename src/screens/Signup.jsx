import React, { useState } from "react";
import { Link } from "react-router-dom";
import { signup, signInWithGoogle } from "../helpers/auth";
import { Formik, Form } from "formik";
import FormTextField from "../components/FormTextField";
import FormButton from "../components/FormButton";
import { loginSchema } from "../helpers/validation";

export default function Signup() {
  const [error, setError] = useState("");

  async function handleSubmit(e) {
    try {
      await signup(e.email, e.password);
    } catch (err) {
      setError(err);
    }
  }
  async function googleSignIn() {
    try {
      await signInWithGoogle();
    } catch (error) {
      setError(error);
    }
  }
  return (
    <div>
      <h1 style={{ marginBottom: 200 }}>Signup</h1>
      <Formik
        initialValues={{ email: "", password: "" }}
        onSubmit={handleSubmit}
        validationSchema={loginSchema}
      >
        {() => (
          <Form>
            <FormTextField name="fullname" label="Full Name" type="text" />
            <FormTextField name="email" label="Email" type="email" />
            <FormTextField name="password" label="Password" type="password" />
            <FormButton />
          </Form>
        )}
      </Formik>
      <p>Or</p>
      <button onClick={googleSignIn} type="button">
        Sign up with Google
      </button>

      {error ? <div>Error </div> : null}
      <div>
        Already have account <Link to="/login">Login</Link>
      </div>
    </div>
  );
}
