import React, { useState, useEffect } from "react";
import { auth } from "../services/firebase";
import { db } from "../services/firebase";
import { Link, useParams } from "react-router-dom";
import { Paper } from "@material-ui/core";
export default function Chat() {
  const [user, setUser] = useState(auth().currentUser);
  const [chats, setChats] = useState([]);
  const [content, setContent] = useState("");
  const [readError, setReadError] = useState(null);
  const [writeError, setWriteError] = useState(null);
  let { id } = useParams();

  useEffect(() => {
    try {
      db.ref("mob-chat").on("value", (snapshot) => {
        let chats = [];
        snapshot.forEach((snap) => {
          chats.push(snap.val());
        });
        setChats(chats);
      });
    } catch (error) {
      setReadError(error.message);
    }
  }, []);

  async function handleSubmit(e) {
    e.preventDefault();
    setWriteError(null);
    try {
      await db.ref("mob-chat").push({
        content: content,
        timestamp: Date.now(),
        uid: user.uid,
        displayName: user.displayName ? user.displayName : user.email,
        toUser: id,
      });
      setContent("");
    } catch (error) {
      setWriteError(error.message);
    }
  }

  const filterChat = (chat) => {
    if(id == 'groupchat') return chat.filter(val => val.toUser == 'groupchat')
    return chat.filter((val) => ((val.uid === id && val.toUser === user.uid ) || (val.uid === user.uid && val.toUser === id)));
  };

  const left = {
    textAlign:'left',
    paddingLeft: 10,
  }
  const right = {
    textAlign:'right',
    paddingRight:10
  }
  return (
    <div>
      <div className="chats" style={{height:'100%'}}>
        {filterChat(chats).map((chat) => {
          return (
            <div>
              <Paper style={ chat.uid == user.uid ? right : left} elevation={3} key={chat.timestamp}>
                <Paper elevation={0}>
                  <small style={{paddingBottom:5, color:'lightgreen'}}>{ chat.uid == user.uid ? "You" : chat.displayName}</small>
                </Paper>
                {chat.content}
              </Paper>
              <br />
            </div>
          );
        })}
      </div>

      <form onSubmit={handleSubmit}>
        <input
          onChange={(e) => setContent(e.target.value)}
          value={content}
          style={{width:'80%', height:50, fontSize:30}}
        ></input>
        {writeError ? <p>{writeError}</p> : null}
        <button style={{width:'10%', height:50, fontSize:30}} type="submit">Send</button>
      </form>
    </div>
  );
}
