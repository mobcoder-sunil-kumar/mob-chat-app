import React, { useState, useEffect } from "react";
import { auth } from "../services/firebase";
import { db } from "../services/firebase";
import { Chat } from "../screens";
import { Route, Link, Switch, useRouteMatch } from "react-router-dom";

const Dashboard = () => {
  const [user, setUser] = useState("");
  const [chats, setChats] = useState([]);
  let { path, url } = useRouteMatch();

  useEffect(() => {
    try {
      db.ref("mob-chat").on("value", (snapshot) => {
        let chats = [];
        snapshot.forEach((snap) => {
          chats.push(snap.val());
        });
        setChats(chats);
      });
    } catch (error) {
      //   setReadError(error.message);
    }
  }, []);

  useEffect(() => {
    auth().onAuthStateChanged((user) => {
      if (user) {
        // setAuthenticated(true);
        //   setLoading(false);
        const displayName = user.displayName;
        setUser(user);
        const email = user.email;
        // const photoURL = user.photoURL;
        // const emailVerified = user.emailVerified;
        const uid = user.uid;
      }
    });
  }, []);

  const getUniqueUser = (chat) => {
    const vals = [];
    return chat.map((val) => {
      if (!vals.includes(val.uid) && val.uid != user.uid) {
        vals.push(val.uid);
        return [val.uid, val.displayName];
      }
      return null;
    });
  };

  return (
    <div>
      <h1>
        Dashboard Welcome - {user.displayName ? user.displayName : user.email}
      </h1>
      <hr />
      <div
        className="chats"
        style={{
          display: "flex",
          width: "100%",
          alignItems: "flex-start",
          minHeight: "100vh",
          marginTop: "0%",
        }}
      >
        <div
          style={{ width: "30%", backgroundColor: "pink", minHeight: "100vh" }}
        >
          <ul>
            <li key="groupchat">
              <Link
                style={{
                  marginBottom: 20,
                  marginTop: 10,
                  textDecoration: "none",
                  display: "block",
                }}
                to={`${url}/groupchat`}
              >
                Group Chat
              </Link>
            </li>
            {getUniqueUser(chats).map((chat) => {
              if (chat) {
                return (
                  <li key={chat[0]}>
                    <Link
                      style={{
                        marginBottom: 20,
                        marginTop: 10,
                        textDecoration: "none",
                        display: "block",
                      }}
                      to={`${url}/${chat[0]}`}
                    >
                      {chat[1]}
                    </Link>
                  </li>
                );
              }
            })}
          </ul>
        </div>
        <div
          style={{
            width: "70%",
            marginLeft: 20,
            marginRight: 29,
            backgroundColor: "pink",
          }}
        >
          <Switch>
            
            <Route path={`${path}/:id`} children={<Chat />} />
          </Switch>
        </div>
      </div>
    </div>
  );
};

export default Dashboard;
