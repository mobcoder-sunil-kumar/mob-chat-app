import React from "react";
import { Paper } from "@material-ui/core";
export default function Home() {
  return (
    <div>
      <Paper elevation={3} style={{fontSize:40}}>Welcome to Mob-chat</Paper>
    </div>
  );
}
