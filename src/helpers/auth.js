import { auth } from "../services/firebase";

// signup function
export function signup(email, password) {
  return auth().createUserWithEmailAndPassword(email, password);
}

//signin function
export function signin(email, password) {
  return auth().signInWithEmailAndPassword(email, password);
}

// signin with google
export function signInWithGoogle() {
    const provider = new auth.GoogleAuthProvider();
    return auth().signInWithPopup(provider);
}

// signout
export function signout() {
  auth().signOut().then(() => alert("logout done!")).catch(() => alert("logout err"))
}