import * as Yup from "yup";
export const loginSchema = Yup.object().shape({
  fullname: Yup.string()
    .required()
    .min(2)
    .max(20)
    .matches(/^[A-Za-z ]*$/, "Please enter valid name"),
  email: Yup.string().email().required(),
  password: Yup.string()
    .required()
    .min(8)
    .matches(/(?=.*?[A-Z])/g, "Should contain a Capital latter")
    .matches(/(?=.*?[a-z])/g, "Should contain a Small latter")
    .matches(/(?=.*?[0-9])/g, "Should contain a Number")
    .matches(/(?=.*?[#?!@$%^&*-])/g, "Should contain a Spacial latter"),
});
